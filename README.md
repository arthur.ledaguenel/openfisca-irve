# Application du modèle OpenFisca sur les normes d'IRVE en résidentiel collectif

L'objectif de ce court projet est la prise en main du fonctionnement d'OpenFisca en l'adaptant à un autre corpus de textes législatifs, en l'occurrence: les normes légales concernant les Infrastructures de Recharge de Véhicules Electriques en résidentiel collectif.

En l'état, le projet permet de renseigner quelques variables clés du parking d'une copropriété et de vérifier sa conformité concernant le prééquipement du parking en puissance disponible pour la recharge de véhicules électriques.

## Installation et test

Pour tester le modèle il suffit de clone le projet Git sur sa machine, de créer un environnement virtuel avec les quelques dépendances mentionnées dans le requirements.txt et d'éxécuter le script test.py depuis la racine du projet.

```bash
conda create --name openfisca_irve
conda activate openfisca_irve
pip install dpath == 1.5.0
pip install psutil == 5.4.7
pip install sortedcontainers == 2.2.2
pip install numexpr == 2.7.0
```

## Structure du projet

Le projet s'articule autours de deux scripts principalement :

* _entities.py_ : créée les classes d'entités (Individus & Copropriétés) sur lesquelles seront ensuite définies les variables. Définit également les relations possibles entre entités (i.e. les différents rôles que peuvent jouer les individus dans la copropriété), fonctionnalité implémentée mais non exploitée pour le moment dans le modèle.
* _variables.py_ : définit les différentes variables primaires (fixées lors du lancement d'une simulation) et secondaires (calculées à partir des variables primaires et secondaires) d'intérêt et implémente les formules de calcul de ces dernières.

Le dossier parameters permet de définir dans des fichiers .yaml certains paramètres d'intéret qui ne dépendent pas de la simulation. Ceci permet de les mettre à jour plus facilement en cas de changement de la législation en vigueur plutôt que d'avoir à modifier le corps du code dans les formule des variables.

Le dossier **openfisca_core** qui contient le moteur de calcul du modèle (et qui est issu directement du repo Git du projet OpenFisca) permet d'effectuer la simulation et n'a pas besoin de modifications.

## Pistes d'approfondissement

L'exactitude légale du modèle n'étant pas le coeur du projet je n'ai pas poussé plus loin la complexité des variables légales. Il est néanmoins facile d'enrichir ce squelette de modèle en ajoutant de nouvelles variables légales en suivant ce canva de base:

```python
class maNouvelleVaraible(Variable):
    value_type = bool/int/float
    entity = Copropriete
    label = "Description de la variable"
    definition_period = YEAR/MONTH/INSTANT
    def formula(copro, period):
        # Note : Explication de la formule si nécessaire.
        # Article sur LégiFrance : https://www.legifrance.gouv.fr/blablabla

        return maNouvelleVaraible
```

Une piste d'approfondissement du modèle (et l'objectif à son commencement) serait de simuler les diverses aides de l'Etat ou des collectivités territoriales disponibles pour les projets d'installation d'IRVE en résidentiel collectif, en fonction des paramètres techniques, financiers et géographiques du projet.

## Références

Le projet est basé sur le projet country-template (disponible [ici](https://github.com/openfisca/country-template)) mis à disposition par OpenFisca ([site web](https://fr.openfisca.org/)) pour que chacun puisse créer un modèle de simulation du système fiscal de son pays.

Il reprend également le moteur de calcul OpenFisca-core qui supporte tous les modèles issus d'OpenFisca. (disponible [ici](https://github.com/openfisca/openfisca-core))

