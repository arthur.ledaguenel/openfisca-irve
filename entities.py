from openfisca_core.entities import build_entity

Individu = build_entity(
    key = "individu",
    plural = "individus",
    label = 'Individu',
    is_person = True
    )

Copropriete = build_entity(
    key = "copro",
    plural = "copros",
    label = 'Copropriété',
    roles = [
        {
            'key': 'resident',
            'plural': 'residents',
            'label': 'Résident',
            'subroles': ['proprietaire', 'locataire']
            },
        {
            'key': 'syndic',
            'plural': 'syndics',
            'label': 'Syndicat des copropriétaires'
            }
        ]
    )

entities = [Copropriete, Individu]