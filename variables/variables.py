from numpy import round, floor, datetime64, maximum

from openfisca_core.periods import MONTH, YEAR
from openfisca_core.variables import Variable
from entities import Copropriete, Individu

class nbrPlaces(Variable):
    value_type = int
    entity = Copropriete
    label = "Nombre de places dans le parking de la copropriété"
    definition_period = YEAR

class puissancePreequipee(Variable):
    value_type = float
    entity = Copropriete
    label = "Puissance prééquipée disponible dans le parking de la copropriété"
    definition_period = YEAR

class puissanceRequise(Variable):
    value_type = float
    entity = Copropriete
    label = "Puissance électrique à prééquiper pour satisfaire les besoins des résidents"
    definition_period = YEAR
    def formula(copro, period, parameters):
        # Note : En prenant en compte un coefficient de foisonnement de 0.4 et une puissance min de prééquipement par place de 7,4 kW.
        # Article sur LégiFrance : https://www.legifrance.gouv.fr/codes/id/LEGIARTI000039803082/2021-03-11/#:~:text=Conform%C3%A9ment%20au%20IV%20de%20l,compter%20du%2011%20mars%202021.
        nb_places = copro('nbrPlaces', period)
        taux = parameters(period).taux_foisonnement

        seuil = nb_places >=10

        return seuil*nb_places*7.4*taux

class infrastructureAuxNormes(Variable):
    value_type = bool
    entity = Copropriete
    label = "L'infrastructure respecte-t-elle ou non les normes en vigueur concernant le prééquipement en puissance du résidentiel collectif"
    definition_period = YEAR
    def formula(copro, period):
        # Note : En prenant en compte un coefficient de foisonnement de 0.4 et une puissance min de prééquipement par place de 7,4 kW.
        # Article sur LégiFrance : https://www.legifrance.gouv.fr/codes/id/LEGIARTI000039803082/2021-03-11/#:~:text=Conform%C3%A9ment%20au%20IV%20de%20l,compter%20du%2011%20mars%202021.

        puissance_preequipee = copro('puissancePreequipee', period)
        puissance_requise = copro('puissanceRequise', period)

        return puissance_preequipee >= puissance_requise